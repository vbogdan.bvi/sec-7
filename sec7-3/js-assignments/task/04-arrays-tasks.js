"use strict";

/*********************************************************************************************
 *                                                                                           *
 * Plese read the following tutorial before implementing tasks:                              *
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array    *
 *                                                                                           *
 * NOTE : Please do not use loops! All tasks can be implmeneted using standard Array methods *
 *                                                                                           *
 *********************************************************************************************/

/**
 * Returns an index of the specified element in array or -1 if element is not found
 *
 * @param {array} arr
 * @param {any} value
 * @return {number}
 *
 * @example
 *    ['Ace', 10, true], 10    => 1
 *    ['Array', 'Number', 'string'], 'Date'    => -1
 *    [0, 1, 2, 3, 4, 5], 5    => 5
 */
function findElement(arr, value) {
  return arr.indexOf(value);
}

/**
 * Generates an array of odd numbers of the specified length
 *
 * @param {number} len
 * @return {array}
 *
 * @example
 *    1 => [ 1 ]
 *    2 => [ 1, 3 ]
 *    5 => [ 1, 3, 5, 7, 9 ]
 */
function generateOdds(len) {
  //   let fr = new Array(len).fill(0);
  //   let result = fr.map(function (elem, index) {
  //     return 2 * index + 1;
  //   });
  //   return result;
  return new Array(len).fill(0).map((el, i) => 2 * i + 1);

}

/**
 * Returns the doubled array - elements of the specified array are repeated twice using original order
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *    ['Ace', 10, true]  => ['Ace', 10, true,   'Ace', 10, true]
 *    [0, 1, 2, 3, 4, 5] => [0, 1, 2, 3, 4, 5,   0, 1, 2, 3, 4, 5]
 *    [] => []
 */
function doubleArray(arr) {
  return arr.concat(arr);
}

/**
 * Returns an array of positive numbers from the specified array in original order
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *    [ 0, 1, 2, 3, 4, 5 ] => [ 1, 2, 3, 4, 5 ]
 *    [-1, 2, -5, -4, 0] => [ 2 ]
 *    [] => []
 */
function getArrayOfPositives(arr) {
  //   let arr1 = [];
  //   for (let i = 0; i < arr.length; i++) {
  //     if (arr[i] > 0) {
  //       arr1.push(arr[i]);
  //     }
  //   }

  // let result = arr.filter(function (elem) {
  //   if (elem > 0) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // });

  return arr.filter(elem => elem > 0);
}

/**
 * Returns the array with strings only in the specified array (in original order)
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *    [ 0, 1, 'cat', 3, true, 'dog' ] => [ 'cat', 'dog' ]
 *    [ 1, 2, 3, 4, 5 ] => []
 *    [ 'cat, 'dog', 'raccon' ] => [ 'cat', 'dog', 'racoon' ]
 */
function getArrayOfStrings(arr) {
  //   let arr1 = [];
  //   for (let i = 0; i < arr.length; i++) {
  //     if (typeof arr[i] === "string") {
  //       arr1.push(arr[i]);
  //     }
  //   }
  //   return arr1;

  // let result = arr.filter(function (elem) {
  //   if (typeof elem === "string") {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // });
  // return result;

  return arr.filter(elem => typeof elem === "string");
}

/**
 * Removes falsy values from the specified array
 * Falsy values: false, null, 0, "", undefined, and NaN.
 * (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean#Description)
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *    [ 0, false, 'cat', NaN, true, '' ] => [ 'cat', true ]
 *    [ 1, 2, 3, 4, 5, 'false' ]         => [ 1, 2, 3, 4, 5, 'false' ]
 *    [ false, 0, NaN, '', undefined ]   => [ ]
 */
function removeFalsyValues(arr) {
  return arr.filter(Boolean);
}

/**
 * Returns the array of useprcase strings from the specified array
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *    [ 'permanent-internship', 'glutinous-shriek', 'multiplicative-elevation' ] => [ 'PERMANENT-INTERNSHIP', 'GLUTINOUS-SHRIEK', 'MULTIPLICATIVE-ELEVATION' ]
 *    [ 'a', 'b', 'c', 'd', 'e', 'f', 'g' ]  => [ 'A', 'B', 'C', 'D', 'E', 'F', 'G' ]
 */
function getUpperCaseStrings(arr) {
  //   let arr1 = [];
  //   for (let i = 0; i < arr.length; i++) {
  //     arr1.push(arr[i].toUpperCase());
  //   }
  //   return arr1;

  // let result = arr.map(function (elem) {
  //   return elem.toUpperCase();
  // });
  // return result;

  return arr.map(elem => elem.toUpperCase());
}

/**
 * Returns the array of string lengths from the specified string array.
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *    [ '', 'a', 'bc', 'def', 'ghij' ]  => [ 0, 1, 2, 3, 4 ]
 *    [ 'angular', 'react', 'ember' ] => [ 7, 5, 5 ]
 */
function getStringsLength(arr) {
  //   let arr1 = [];
  //   for (let i = 0; i < arr.length; i++) {
  //     arr1.push(arr[i].length);
  //   }
  //   return arr1;

  // let result = arr.map(function (elem) {
  //   return elem.length;
  // });
  // return result;

  return arr.map(elem => elem.length);
}

/**
 * Inserts the item into specified array at specified index
 *
 * @param {array} arr
 * @param {any} item
 * @param {number} index
 *
 * @example
 *    [ 1, 3, 4, 5 ], 2, 1  => [ 1, 2, 3, 4, 5 ]
 *    [ 1, 'b', 'c'], 0, 'x'  => [ 'x', 1, 'b', 'c' ]
 */
function insertItem(arr, item, index) {
  arr.splice(index, 0, item);
  return arr;
}

/**
 * Returns the n first items of the specified array
 *
 * @param {array} arr
 * @param {number} n
 *
 * @example
 *    [ 1, 3, 4, 5 ], 2  => [ 1, 2 ]
 *    [ 'a', 'b', 'c', 'd'], 3  => [ 'a', 'b', 'c' ]
 */
function getHead(arr, n) {
  return arr.slice(0, n);
}

/**
 * Returns the n last items of the specified array
 *
 * @param {array} arr
 * @param {number} n
 *
 * @example
 *    [ 1, 3, 4, 5 ], 2  => [ 4, 5 ]
 *    [ 'a', 'b', 'c', 'd'], 3  => [ 'b', 'c', 'd' ]
 */
function getTail(arr, n) {
  return arr.slice(-n);
}

/**
 * Returns CSV represebtation of two-dimentional numeric array.
 * https://en.wikipedia.org/wiki/Comma-separated_values
 *
 * @param {array} arr
 * @return {string}
 *
 * @example
 *    [
 *       [  0, 1, 2, 3, 4 ],
 *       [ 10,11,12,13,14 ],
 *       [ 20,21,22,23,24 ],
 *       [ 30,31,32,33,34 ]
 *    ]
 *           =>
 *     '0,1,2,3,4\n'
 *    +'10,11,12,13,14\n'
 *    +'20,21,22,23,24\n'
 *    +'30,31,32,33,34'
 */
function toCsvText(arr) {
  // let s1 = "";
  // for (let i = 0; i < arr.length; i++) {
  //   for (let j = 0; j < arr[i].length; j++) {
  //     s1 = s1 + arr[i][j] + ",";
  //   }
  //   s1 = s1.slice(0, -1);
  //   s1 = s1 + "\n";
  // }
  // s1 = s1.slice(0, -1);
  // return s1;

  // let result = arr.map(function (elem) {
  //   let s = elem.join(",");
  //   return s;
  // });
  // let s = result.join("\n");
  // return s;

  return arr.map(elem => elem.join(',')).join('\n');
}

/**
 * Transforms the numeric array into the according array of squares:
 *   f(x) = x * x
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *   [ 0, 1, 2, 3, 4, 5 ] => [ 0, 1, 4, 9, 16, 25 ]
 *   [ 10, 100, -1 ]      => [ 100, 10000, 1 ]
 */
function toArrayOfSquares(arr) {
  // let arr1 = [];
  // for (let i = 0; i < arr.length; i++) {
  //   arr1.push(arr[i] * arr[i]);
  // }
  // return arr1;

  // let res = arr.map(function (elem) {
  //   return elem * elem;
  // });
  // return res;

  return arr.map(elem => elem * elem);
}

/**
 * Transforms the numeric array to the according moving sum array:
 *     f[n] = x[0] + x[1] + x[2] +...+ x[n]
 *  or f[n] = f[n-1] + x[n]
 *
 * @param {array} arr
 * @return {array}
 *
 * Example :
 *   [ 1, 1, 1, 1, 1 ]        => [ 1, 2, 3, 4, 5 ]
 *   [ 10, -10, 10, -10, 10 ] => [ 10, 0, 10, 0, 10 ]
 *   [ 0, 0, 0, 0, 0]         => [ 0, 0, 0, 0, 0]
 *   [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] => [ 1, 3, 6, 10, 15, 21, 28, 36, 45, 55 ]
 */
function getMovingSum(arr) {
  // let arr1 = [];
  // for (let i = 0; i < arr.length; i++) {
  //   let k = 0;
  //   for (let j = 0; j <= i; j++) {
  //     k = k + arr[j];
  //   }
  //   arr1.push(k);
  // }
  // return arr1;

  // let k = 0;
  // let res = arr.map(function (elem) {
  //   k = k + elem;
  //   return k;
  // });
  // return res;

  let k = 0
  return arr.map(elem => k += elem);
}

/**
 * Returns every second item from the specified array:
 *
 * @param {array} arr
 * @return {array}
 *
 * Example :
 * [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] => [ 2, 4, 6, 8, 10 ]
 * [ 'a', 'b', 'c' , null ]  => [ "b", null ]
 * [ "a" ] => []
 */
function getSecondItems(arr) {
  // let arr1 = [];
  // for (let i = 1; i < arr.length; i = i + 2) {
  //   arr1.push(arr[i]);
  // }
  // return arr1;

  // let res = arr.filter(function (elem, i) {
  //   return i % 2 == 1;
  // });
  // return res;

  return arr.filter((elem, i) => i % 2 === 1)
}

/**
 * Propagates every item in sequence its position times
 * Returns an array that consists of: one first item, two second items, tree third items etc.
 *
 * @param {array} arr
 * @return {array}
 *
 * @example :
 *  [] => []
 *  [ 1 ] => [ 1 ]
 *  [ 'a', 'b' ] => [ 'a', 'b','b' ]
 *  [ 'a', 'b', 'c', null ] => [ 'a', 'b','b', 'c','c','c',  null,null,null,null ]
 *  [ 1,2,3,4,5 ] => [ 1, 2,2, 3,3,3, 4,4,4,4, 5,5,5,5,5 ]
 */
function propagateItemsByPositionIndex(arr) {
  // let arr1 = [];
  // for (let i = 0; i < arr.length; i++) {
  //   let k = 0;
  //   for (let j = 0; j <= i; j++) {
  //     arr1.push(arr[i]);
  //   }
  // }
  // return arr1;

  // let res = arr.map(function (elem, i) {
  //   return new Array(i + 1).fill(elem);
  // });

  let res = arr.map((elem, i) => new Array(i + 1).fill(elem));
  res = res.flat();
  return res;
}

/**
 * Returns the 3 largest numbers from the specified array
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *   [] => []
 *   [ 1, 2 ] => [ 2, 1 ]
 *   [ 1, 2, 3 ] => [ 3, 2, 1 ]
 *   [ 1,2,3,4,5,6,7,8,9,10 ] => [ 10, 9, 8 ]
 *   [ 10, 10, 10, 10 ] => [ 10, 10, 10 ]
 */
function get3TopItems(arr) {
  // arr.sort(function (a, b) {
  //   return b - a;
  // });  

  arr.sort((a, b) => b - a);
  let arr1 = arr.slice(0, 3);
  return arr1;
}

/**
 * Returns the number of positive numbers from specified array
 *
 * @param {array} arr
 * @return {number}
 *
 * @example
 *   [ ]          => 0
 *   [ -1, 0, 1 ] => 1
 *   [ 1, 2, 3]   => 3
 *   [ null, 1, 'elephant' ] => 1
 *   [ 1, '2' ] => 1
 */
function getPositivesCount(arr) {
  // let n = 0;
  // for (let i = 0; i < arr.length; i++) {
  //   if (arr[i] > 0 && typeof arr[i] === "number") {
  //     n++;
  //   }
  // }
  // return n;

  let res = arr.filter((elem) => (typeof elem === "number" && elem > 0));

  return res.length;
}

/**
 * Sorts digit names
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *   [] => []
 *   [ 'nine','one' ]                 => [ 'one', 'nine' ]
 *   [ 'one','two','three' ]          => [ 'one','two', 'three' ]
 *   [ 'nine','eight','nine','eight'] => [ 'eight','eight','nine','nine']
 *   [ 'one','one','one','zero' ]     => [ 'zero','one','one','one' ]
 */
function sortDigitNamesByNumericOrder(arr) {
  let map1 = new Map();

  map1.set("zero", 0);
  map1.set("one", 1);
  map1.set("two", 2);
  map1.set("three", 3);
  map1.set("four", 4);
  map1.set("five", 5);
  map1.set("six", 6);
  map1.set("seven", 7);
  map1.set("eight", 8);
  map1.set("nine", 9);

  arr.sort((a, b) => map1.get(a) - map1.get(b));
  return arr;
}

/**
 * Returns the sum of all items in the specified array of numbers
 *
 * @param {array} arr
 * @return {number}
 *
 * @example
 *   [] => 0
 *   [ 1, 2, 3 ]           => 6
 *   [ -1, 1, -1, 1 ]      => 0
 *   [ 1, 10, 100, 1000 ]  => 1111
 */
function getItemsSum(arr) {
  // let sum = 0;
  // for (let i = 0; i < arr.length; i++) {
  //   sum = sum + arr[i];
  // }
  // return sum;
  if (arr.length === 0) return 0;
  let sum = arr.reduce(function (previousValue, currentValue) {
    return previousValue + currentValue;
  });
  return sum;
}

/**
 * Returns the number of all falsy value in the specified array
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *  [] => 0
 *  [ 1, '', 3 ] => 1
 *  [ -1, 'false', null, 0 ] => 2
 *  [ null, undefined, NaN, false, 0, '' ]  => 6
 */
function getFalsyValuesCount(arr) {
  let stN = arr.length;

  arr = arr.filter(String);
  arr = arr.filter(Boolean);
  return stN - arr.length;
}

/**
 * Returns a number of all occurences of the specified item in an array
 *
 * @param {array} arr
 * @param {any} item
 * @return {number}
 *
 * @example
 *    [ 0, 0, 1, 1, 1, 2 ], 1 => 3
 *    [ 1, 2, 3, 4, 5 ], 0 => 0
 *    [ 'a','b','c','c' ], 'c'=> 2
 *    [ null, undefined, null ], null => 2
 *    [ true, 0, 1, 'true' ], true => 1
 */
function findAllOccurences(arr, item) {
  var filtered = arr.filter((el) => el === item);
  return filtered.length;
}

/**
 * Concatenates all elements from specified array into single string with ',' delimeter
 *
 * @param {array} arr
 * @return {string}
 *
 * @example
 *    [0, false, 'cat', NaN, true, '']  => '0,false,cat,NaN,true,'
 *    [1, 2, 3, 4, 5]                   => '1,2,3,4,5'
 *    ['rock', 'paper', 'scissors']     => 'rock,paper,scissors'
 */
function toStringList(arr) {
  return arr.join(",");
}

/**
 * Sorts the specified array by country name first and city name (if countries are equal) in ascending order.
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *    [
 *      { country: 'Russia',  city: 'Moscow' },
 *      { country: 'Belarus', city: 'Minsk' },
 *      { country: 'Poland',  city: 'Warsaw' },
 *      { country: 'Russia',  city: 'Saint Petersburg' },
 *      { country: 'Poland',  city: 'Krakow' },
 *      { country: 'Belarus', city: 'Brest' }
 *    ]
 *                      =>
 *    [
 *      { country: 'Belarus', city: 'Brest' },
 *      { country: 'Belarus', city: 'Minsk' },
 *      { country: 'Poland',  city: 'Krakow' },
 *      { country: 'Poland',  city: 'Warsaw' },
 *      { country: 'Russia',  city: 'Moscow' },
 *      { country: 'Russia',  city: 'Saint Petersburg' }
 */
function sortCitiesArray(arr) {
  arr = arr.sort(function (a, b) {
    let aCountry = a.country.toLowerCase();
    let bCountry = b.country.toLowerCase();
    let bCity = b.city.toLowerCase();
    let aCity = a.city.toLowerCase();

    if (aCountry === bCountry) {
      if (aCity > bCity) {
        return 1;
      } else {
        return -1;
      }
    } else if (aCountry > bCountry) {
      return 1;
    } else {
      return -1;
    }

  });
  return arr;
}

/**
 * Creates an indentity matrix of the specified size
 *
 * @param {number} n
 * @return {array}
 *
 * @example
 *     1  => [[1]]
 *
 *     2 => [[1,0],
 *           [0,1]]
 *
 *          [[1,0,0,0,0],
 *           [0,1,0,0,0],
 *     5 =>  [0,0,1,0,0],
 *           [0,0,0,1,0],
 *           [0,0,0,0,1]]
 */
function getIdentityMatrix(n) {
  // let arr = [];
  // for (let i = 0; i < n; i++) {
  //   let arr1 = [];
  //   for (let j = 0; j < n; j++) {
  //     if (j == i) {
  //       arr1.push(1);
  //     } else {
  //       arr1.push(0);
  //     }
  //   }
  //   arr.push(arr1);
  // }
  // return arr;

  let arr1 = new Array(n).fill().map(function (elem, i) {
    elem = new Array(n).fill(0);
    elem[i] = 1;
    return elem;
  });
  return arr1;
}

/**
 * Creates an array of integers from the specified start to end (inclusive)
 *
 * @param {number} start
 * @param {number} end
 * @return {array}
 *
 * @example
 *     1, 5  => [ 1, 2, 3, 4, 5 ]
 *    -2, 2  => [ -2, -1, 0, 1, 2 ]
 *     0, 100 => [ 0, 1, 2, ..., 100 ]
 *     3, 3   => [ 3 ]
 */
function getIntervalArray(start, end) {
  // let arr = [];
  // for (let i = start; i <= end; i++) {
  //   arr.push(i);
  // }
  // return arr;

  // let arr = new Array(end - start + 1).fill();
  // arr = arr.map(function (elem) {
  //   elem = start;
  //   start++;
  //   return elem;
  // });
  // return arr;

  let arr = new Array(end - start + 1).fill();
  return arr.map(elem => start++);
}

/**
 * Returns array containing only unique values from the specified array.
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *   [ 1, 2, 3, 3, 2, 1 ] => [ 1, 2, 3 ]
 *   [ 'a', 'a', 'a', 'a' ]  => [ 'a' ]
 *   [ 1, 1, 2, 2, 3, 3, 4, 4] => [ 1, 2, 3, 4]
 */
function distinct(arr) {
  let mySet = new Set(arr);
  let myArr = Array.from(mySet);
  return myArr;
}

/**
 * Groups elements of the specified array by key.
 * Returns multimap of keys extracted from array elements via keySelector callback
 * and values extracted via valueSelector callback.
 * See: https://en.wikipedia.org/wiki/Multimap
 *
 * @param {array} array
 * @param {Function} keySelector
 * @param {Function} valueSelector
 * @return {Map}
 *
 * @example
 *   group([
 *      { country: 'Belarus', city: 'Brest' },
 *      { country: 'Russia', city: 'Omsk' },
 *      { country: 'Russia', city: 'Samara' },
 *      { country: 'Belarus', city: 'Grodno' },
 *      { country: 'Belarus', city: 'Minsk' },
 *      { country: 'Poland', city: 'Lodz' }
 *     ],
 *     item => item.country,
 *     item => item.city
 *   )
 *            =>
 *   Map {
 *    "Belarus" => ["Brest", "Grodno", "Minsk"],
 *    "Russia" => ["Omsk", "Samara"],
 *    "Poland" => ["Lodz"]
 *   }
 */
function group(array, keySelector, valueSelector) {
  // let map = new Map(),
  //   count = array.length;

  // for (let key, i = 0; i < count; ++i) {
  //   key = keySelector(array[i]);
  //   if (map.has(key)) {
  //     map.get(key).push(valueSelector(array[i]));
  //   } else {
  //     map.set(key, [valueSelector(array[i])]);
  //   }
  // }
  // return map;

  let mapRes = new Map();
  array.map(function (elem) {
    let key = keySelector(elem);
    if (mapRes.has(key)) {
      mapRes.get(key).push(valueSelector(elem));

    } else {
      mapRes.set(key, [valueSelector(elem)]);
    }
  });
  return mapRes;
}

/**
 * Projects each element of the specified array to a sequence and flattens the resulting sequences into one array.
 *
 * @param {array} arr
 * @param {Function} childrenSelector, a transform function to apply to each element that returns an array of children
 * @return {array}
 *
 * @example
 *   [[1, 2], [3, 4], [5, 6]], (x) => x     =>   [ 1, 2, 3, 4, 5, 6 ]
 *   ['one','two','three'], x=>x.split('')  =>   ['o','n','e','t','w','o','t','h','r','e','e']
 */
function selectMany(arr, childrenSelector) {
  return arr.flatMap(childrenSelector);
}

/**
 * Returns an element from the multidimentional array by the specified indexes.
 *
 * @param {array} arr
 * @param {array} indexes
 * @return {any} element from array
 *
 * @example
 *   [[1, 2], [3, 4], [5, 6]], [0,0]  => 1        (arr[0][0])
 *   ['one','two','three'], [2]       => 'three'  (arr[2])
 *   [[[ 1, 2, 3]]], [ 0, 0, 1 ]      => 2        (arr[0][0][1])
 */
function getElementByIndexes(arr, indexes) {
  if (indexes.length > 1) {
    let j = indexes[0];
    indexes = indexes.slice(1);
    return getElementByIndexes(arr[j], indexes);
  } else {
    return arr[indexes[0]];
  }
}

/**
 * Swaps the head and tail of the specified array:
 * the head (first half) of array move to the end, the tail (last half) move to the start.
 * The middle element (if exists) leave on the same position.
 *
 *
 * @param {array} arr
 * @return {array}
 *
 * @example
 *   [ 1, 2, 3, 4, 5 ]   =>  [ 4, 5, 3, 1, 2 ]
 *    \----/   \----/
 *     head     tail
 *
 *   [ 1, 2 ]  => [ 2, 1 ]
 *   [ 1, 2, 3, 4, 5, 6, 7, 8 ]   =>  [ 5, 6, 7, 8, 1, 2, 3, 4 ]
 *
 */
function swapHeadAndTail(arr) {
  let len = Math.floor(arr.length / 2);
  let arr1 = [];
  if (arr.length <= 1) {
    return arr;
  } else if (arr.length % 2 == 1) {
    arr1 = arr.slice(-len).concat(arr[len]);
    arr1 = arr1.concat(arr.slice(0, len));
  } else {
    arr1 = arr.slice(-len).concat(arr.slice(0, len));
  }
  return arr1;
}

module.exports = {
  findElement: findElement,
  generateOdds: generateOdds,
  doubleArray: doubleArray,
  getArrayOfPositives: getArrayOfPositives,
  getArrayOfStrings: getArrayOfStrings,
  removeFalsyValues: removeFalsyValues,
  getUpperCaseStrings: getUpperCaseStrings,
  getStringsLength: getStringsLength,
  insertItem: insertItem,
  getHead: getHead,
  getTail: getTail,
  toCsvText: toCsvText,
  toStringList: toStringList,
  toArrayOfSquares: toArrayOfSquares,
  getMovingSum: getMovingSum,
  getSecondItems: getSecondItems,
  propagateItemsByPositionIndex: propagateItemsByPositionIndex,
  get3TopItems: get3TopItems,
  getPositivesCount: getPositivesCount,
  sortDigitNamesByNumericOrder: sortDigitNamesByNumericOrder,
  getItemsSum: getItemsSum,
  getFalsyValuesCount: getFalsyValuesCount,
  findAllOccurences: findAllOccurences,
  sortCitiesArray: sortCitiesArray,
  getIdentityMatrix: getIdentityMatrix,
  getIntervalArray: getIntervalArray,
  distinct: distinct,
  group: group,
  selectMany: selectMany,
  getElementByIndexes: getElementByIndexes,
  swapHeadAndTail: swapHeadAndTail,
};
