/*

        1) Написать	функцию,	которая	принимает два	входных параметра	– один числовой массив,	а	
        второй	- числовое	значение,	и возвращает	индекс	значения в	массиве.	Если	такого	нет,	
        вернуть	 -1.

*/

function getIndexOfValueInArray(arr, value) {
  return arr.indexOf(value);
}

console.log("----------sec7-1-1----------");

let arr1 = [1, 0, 2, 3, 5, false];
console.log(arr1);

console.log("index of \"0\" = " + getIndexOfValueInArray(arr1, 0));

/*
        2) Написать	функцию,	принимающую	один	входной	параметр	(массив),	которая	сортирует	этот	
        массив	по	возрастанию.
*/
console.log("----------sec7-1-2----------");

function compareNumeric(a, b) {
  if (a > b) return 1;
  if (a == b) return 0;
  if (a < b) return -1;
}

let arr2 = [1, 2, 15, 20, 0, 9];

arr2.sort(compareNumeric);
// arr2.sort((a, b) => a - b);
console.log("compare numeric");
console.log(arr2);

arr2.sort();
console.log("compare alphabetical");
console.log(arr2);

/*
        3) Написать	функцию,	которая принимает	два	объекта	и	сравнивает	между	собой	их	поля	и	
        значения.	Если	объекты	поля	и	значения	объектов	идентичны,	то функция	возвращает	true,	
        иначе	false.
  */
console.log("----------sec7-1-3----------");

var myCar = new Object();
myCar.make = "Ford";
myCar.model = "Mustang";
myCar.year = 1969;
myCar.engine = {
  cylinders: 4,
  size: 2.2,
};

var myCar1 = new Object();
myCar1.make = "Ford";
myCar1.model = "Mustang";
myCar1.year = 1969;
myCar1.engine = {
  cylinders: 4,
  size: 2.2,
};

var myCar2 = new Object();
myCar2.make = "Ford";
myCar2.model = "Mustang";
myCar2.year = 1969;
myCar2.color = "black";

var myCar3 = new Object();
myCar3.make = "Ford";
myCar3.model = "Mustang";
myCar3.year = 1969;
myCar3.engine = {
  cylinders: 5,
  size: 2.5,
};

var myCar4;
var myCar5;

console.log(myCar, myCar1);
console.log(object_equals(myCar, myCar1));

console.log(myCar, myCar2);
console.log(object_equals(myCar, myCar2));

console.log(myCar, myCar3);
console.log(object_equals(myCar, myCar3));

console.log(myCar, myCar4);
console.log(object_equals(myCar, myCar4));

console.log(myCar5, myCar4);
console.log(object_equals(myCar5, myCar4));

function object_equals(x, y) {
  if (x === y) return true;
  // if both x and y are null or undefined and exactly the same

  if (!(x instanceof Object) || !(y instanceof Object)) return false;
  // if they are not strictly equal, they both need to be Objects

  if (x.constructor !== y.constructor) return false;
  // they must have the exact same prototype chain, the closest we can do is
  // test there constructor.

  for (var p in x) {
    if (!x.hasOwnProperty(p)) continue;
    // other properties were tested using x.constructor === y.constructor

    if (!y.hasOwnProperty(p)) return false;
    // allows to compare x[ p ] and y[ p ] when set to undefined

    if (x[p] === y[p]) continue;
    // if they have the same strict value or identity then they are equal

    if (typeof x[p] !== "object") return false;
    // Numbers, Strings, Functions, Booleans must be strictly equal

    if (!object_equals(x[p], y[p])) return false;
    // Objects and Arrays must be tested recursively
  }

  for (p in y) if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) return false;
  // allows x[ p ] to be set to undefined

  return true;
}

/*
        4) Написать	функцию (getRectangle()),	которая принимает	два	параметра(width	и	height)	и
        возвращает	объект	у	которого	есть	2	поля	заполненные	значениями	width	и	height,	а	так	же	
        метод getArea(),	который	возвращает	width * height.
*/
console.log("----------sec7-1-4----------");

function Rectangle(width, height) {
  this.width = width;
  this.height = height;
  this.getArea = function () {
    return this.width * this.height;
  };
}

function getRectangle(width, height) {
  return new Rectangle(width, height);
}

let rec = getRectangle(5, 8);
console.log(rec);
console.log(rec.getArea());

console.log("--------------------------");
